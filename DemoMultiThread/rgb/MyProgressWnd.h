#pragma once

#include "MyFifo.h"
#include "Progress_img.h"

// MyProgressWnd

class MyProgressWnd : public CWnd
{
	DECLARE_DYNAMIC(MyProgressWnd)

public:
	void create(void);
	void update(int pos1, int pos2, int max);
	virtual ~MyProgressWnd();
	Progress_Img m_Img;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
};


