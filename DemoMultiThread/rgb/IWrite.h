#pragma once

#include "Image_NSU.h"

class IWrite
{
public:
	virtual void write(int &count) = 0;
};

