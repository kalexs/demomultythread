
// rgbDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "rgb.h"
#include "rgbDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// ���������� ���� CrgbDlg



CrgbDlg::CrgbDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CrgbDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CrgbDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER1, m_SlideR);
	DDX_Control(pDX, IDC_EDIT1, m_EditR);
	DDX_Control(pDX, IDC_SPIN1, m_SpinR);
	DDX_Control(pDX, IDC_EDIT2, m_InformBox);
	DDX_Control(pDX, IDC_SLIDER2, m_SlideG);
	DDX_Control(pDX, IDC_EDIT3, m_EditG);
	DDX_Control(pDX, IDC_SPIN2, m_SpinG);
	DDX_Control(pDX, IDC_SLIDER3, m_SlideB);
	DDX_Control(pDX, IDC_EDIT4, m_EditB);
	DDX_Control(pDX, IDC_SPIN3, m_SpinB);
	DDX_Control(pDX, IDC_PLACE, m_Paint);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress_Scale);
	DDX_Control(pDX, IDC_EDIT5, m_RewriteText);
	DDX_Control(pDX, IDC_EDIT6, m_RereadText);
	DDX_Control(pDX, IDC_PLACE2, m_Progress);
}

BEGIN_MESSAGE_MAP(CrgbDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_TIMER()
	ON_EN_CHANGE(IDC_EDIT1, &CrgbDlg::OnEnChangeEdit1)
	ON_EN_CHANGE(IDC_EDIT3, &CrgbDlg::OnEnChangeEdit3)
	ON_EN_CHANGE(IDC_EDIT4, &CrgbDlg::OnEnChangeEdit4)
	ON_STN_CLICKED(IDC_PLACE, &CrgbDlg::OnStnClickedPlace)
END_MESSAGE_MAP()


// ����������� ��������� CrgbDlg

BOOL CrgbDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ���������� ������ "� ���������..." � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����.  ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������

	// TODO: �������� �������������� �������������

	m_SlideR.SetRange(0, 255);
	m_SlideR.SetPageSize(10);
	m_SlideR.SetPos(0);

	m_SpinR.SetRange(0, 255);
	m_SpinR.SetPos(0);
	//m_SpinR.SetBuddy(&m_EditR);

	m_SlideG.SetRange(0, 255);
	m_SlideG.SetPageSize(10);
	m_SlideG.SetPos(0);

	m_SpinG.SetRange(0, 255);
	m_SpinG.SetPos(0);
	//m_SpinG.SetBuddy(&m_EditG);

	m_SlideB.SetRange(0, 255);
	m_SlideB.SetPageSize(10);
	m_SlideB.SetPos(0);

	m_SpinB.SetRange(0, 255);
	m_SpinB.SetPos(0);
	//m_SpinB.SetBuddy(&m_EditB);


	CString str;
	str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());

	m_EditR.SetWindowTextW(_T("0"));
	m_EditG.SetWindowTextW(_T("0"));
	m_EditB.SetWindowTextW(_T("0"));

	m_RereadText.SetWindowTextW(_T("0"));
	m_RewriteText.SetWindowTextW(_T("0"));

	m_InformBox.SetWindowTextW(str);


	::SetTimer(m_hWnd, 1, 200, NULL);
	m_Paint.create();
	m_Progress.create();

	m_Progress_Scale.SetRange(0, m_Paint.m_fifo.m_frame_num);
	m_Progress_Scale.SetPos(m_Paint.m_fifo.Stats());

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CrgbDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������.  ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CrgbDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CrgbDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CrgbDlg::OnTimer(UINT id)
{
	if (id == 1)
	{
		m_Progress_Scale.SetPos(m_Paint.m_fifo.Stats());
		CString str;
		str.Format(_T("%d"), m_Paint.m_fifo.m_read);
		m_RereadText.SetWindowTextW(str);
		str.Format(_T("%d"), m_Paint.m_fifo.m_write);
		m_RewriteText.SetWindowTextW(str);
		m_Paint.Invalidate(TRUE);
		m_Progress.update(m_Paint.m_fifo.m_read, m_Paint.m_fifo.m_write, m_Paint.m_fifo.m_frame_num);
		m_Progress.Invalidate(TRUE);
	}
}

void CrgbDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: �������� ���� ��� ����������� ��������� ��� ����� ������������
	CString str;
	int n = 0;
	if (NULL == pScrollBar) {}
	else if (pScrollBar->m_hWnd == m_SlideR.m_hWnd)
	{
		n = m_SlideR.GetPos();
		switch (nSBCode) {
		case SB_LEFT:			n = 0;		break;
		case SB_RIGHT:			n = 100;	break;
		case SB_LINELEFT:		n--;		break;
		case SB_LINERIGHT:		n++;		break;
		case SB_PAGELEFT:		n -= 10;	break;
		case SB_PAGERIGHT:		n += 10;	break;
		case SB_THUMBTRACK:
		case SB_THUMBPOSITION:	n = nPos;	break;
		};
		m_SlideR.SetPos(n);
		str.Format(_T("%i"), n);
		m_EditR.SetWindowTextW(str);
		str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());
		m_InformBox.SetWindowTextW(str);
		m_SpinR.SetPos(n);
	}


	else if (pScrollBar->m_hWnd == m_SlideG.m_hWnd)
	{
		n = m_SlideG.GetPos();
		switch (nSBCode) {
		case SB_LEFT:			n = 0;		break;
		case SB_RIGHT:			n = 100;	break;
		case SB_LINELEFT:		n--;		break;
		case SB_LINERIGHT:		n++;		break;
		case SB_PAGELEFT:		n -= 10;	break;
		case SB_PAGERIGHT:		n += 10;	break;
		case SB_THUMBTRACK:
		case SB_THUMBPOSITION:	n = nPos;	break;
		};
		m_SlideG.SetPos(n);
		str.Format(_T("%i"), n);
		m_EditG.SetWindowTextW(str);
		str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());
		m_InformBox.SetWindowTextW(str);
		m_SpinG.SetPos(n);
	}

	else if (pScrollBar->m_hWnd == m_SlideB.m_hWnd)
	{
		n = m_SlideB.GetPos();
		switch (nSBCode) {
		case SB_LEFT:			n = 0;		break;
		case SB_RIGHT:			n = 100;	break;
		case SB_LINELEFT:		n--;		break;
		case SB_LINERIGHT:		n++;		break;
		case SB_PAGELEFT:		n -= 10;	break;
		case SB_PAGERIGHT:		n += 10;	break;
		case SB_THUMBTRACK:
		case SB_THUMBPOSITION:	n = nPos;	break;
		};
		m_SlideB.SetPos(n);
		str.Format(_T("%i"), n);
		m_EditB.SetWindowTextW(str);
		str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());
		m_InformBox.SetWindowTextW(str);
		m_SpinB.SetPos(n);
	}
	draw_pict(str);
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CrgbDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO: �������� ���� ��� ����������� ��������� ��� ����� ������������
	CString str;
	int t = nPos;
	if (NULL == pScrollBar) {}
	else if (pScrollBar->m_hWnd == m_SpinR.m_hWnd)
	{
		//t = m_SpinR.GetPos();
		switch (nSBCode) {
		case SB_LINELEFT:		t--;		break;
		case SB_LINERIGHT:		t++;		break;
		case SB_THUMBTRACK:
		case SB_THUMBPOSITION:	t = nPos;	break;
		};
		m_SpinR.SetPos(t);
		str.Format(_T("%i"), t);
		m_EditR.SetWindowTextW(str);
		m_EditR.SetWindowTextW(str);
		str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());
		m_InformBox.SetWindowTextW(str);
		m_SlideR.SetPos(t);
	}

	else if (pScrollBar->m_hWnd == m_SpinG.m_hWnd)
	{
		//t = m_SpinR.GetPos();
		switch (nSBCode) {
		case SB_LINELEFT:		t--;		break;
		case SB_LINERIGHT:		t++;		break;
		case SB_THUMBTRACK:
		case SB_THUMBPOSITION:	t = nPos;	break;
		};
		m_SpinG.SetPos(t);
		str.Format(_T("%i"), t);
		m_EditG.SetWindowTextW(str);
		m_EditG.SetWindowTextW(str);
		str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());
		m_InformBox.SetWindowTextW(str);
		m_SlideG.SetPos(t);
	}

	else if (pScrollBar->m_hWnd == m_SpinB.m_hWnd)
	{
		//t = m_SpinR.GetPos();
		switch (nSBCode) {
		case SB_LINELEFT:		t--;		break;
		case SB_LINERIGHT:		t++;		break;
		case SB_THUMBTRACK:
		case SB_THUMBPOSITION:	t = nPos;	break;
		};
		m_SpinB.SetPos(t);
		str.Format(_T("%i"), t);
		m_EditB.SetWindowTextW(str);
		m_EditB.SetWindowTextW(str);
		str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());
		m_InformBox.SetWindowTextW(str);
		m_SlideB.SetPos(t);
	}
	draw_pict(str);
	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CrgbDlg::OnEnChangeEdit1()
{
	int n;
	CString str;
	m_EditR.GetWindowTextW(str);
	n = _tstoi(str);
	if (n > 255) { n = 255; str.Format(_T("%i"), n); m_EditR.SetWindowTextW(str);}
	if (n < 0) { n = 0; str.Format(_T("%i"), n); m_EditR.SetWindowTextW(str);}
	str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());
	m_SlideR.SetPos(n);
	m_SpinR.SetPos(n);
	draw_pict(str);
}

void CrgbDlg::OnEnChangeEdit3()
{
	int n;
	CString str;
	m_EditG.GetWindowTextW(str);
	n = _tstoi(str);
	if (n > 255) { n = 255; str.Format(_T("%i"), n); m_EditG.SetWindowTextW(str); }
	if (n < 0) { n = 0; str.Format(_T("%i"), n); m_EditG.SetWindowTextW(str); }
	str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());
	m_SlideG.SetPos(n);
	m_SpinG.SetPos(n);
	draw_pict(str);
}


void CrgbDlg::OnEnChangeEdit4()
{
	int n;
	CString str;
	m_EditB.GetWindowTextW(str);
	n = _tstoi(str);
	if (n > 255) { n = 255; str.Format(_T("%i"), n); m_EditB.SetWindowTextW(str); }
	if (n < 0) { n = 0; str.Format(_T("%i"), n); m_EditB.SetWindowTextW(str); }
	str.Format(_T("R = %d, G = %d, B = %d"), m_SlideR.GetPos(), m_SlideG.GetPos(), m_SlideB.GetPos());
	m_SlideB.SetPos(n);
	m_SpinB.SetPos(n);
	draw_pict(str);
}

void CrgbDlg::draw_pict(CString str)
{
	//m_InformBox.SetWindowTextW(str);
	//m_Paint.m_RED = (BYTE)m_SlideR.GetPos();
	//m_Paint.m_GREEN = (BYTE)m_SlideG.GetPos();
	//m_Paint.m_BLUE = (BYTE)m_SlideB.GetPos();
	//m_Paint.Invalidate(TRUE);
	
	m_Paint.m_writer.m_dwTimeOut = m_SlideR.GetPos();
	::KillTimer(m_hWnd, 1);
	::SetTimer(m_hWnd, 1, m_SlideG.GetPos(), NULL);
}


void CrgbDlg::OnStnClickedPlace()
{
	// TODO: �������� ���� ��� ����������� �����������
}
