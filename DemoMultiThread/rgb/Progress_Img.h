#pragma once

class Progress_Img
{
public:
	Progress_Img()
	{
		m_pData = NULL;
	};

	~Progress_Img()
	{
		if (NULL != m_pData)
		{
			delete[] m_pData;
			m_pData = NULL;
		}
	};

	void fill(float pos1, float pos2, float max);
	void draw(CPaintDC *pDC);
	void create(int w, int h);
private:
	COLORREF *m_pData;
	BITMAPINFOHEADER m_bmp;
};
