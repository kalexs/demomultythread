// MyPaintWnd.cpp: ���� ����������
//

#include "stdafx.h"
#include "rgb.h"
#include "MyPaintWnd.h"


// MyPaintWnd

IMPLEMENT_DYNAMIC(MyPaintWnd, CWnd)


MyPaintWnd::~MyPaintWnd()
{
}


BEGIN_MESSAGE_MAP(MyPaintWnd, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()

// ����������� ��������� MyPaintWnd

void  MyPaintWnd::create(void)
{
	CRect rc;
	GetClientRect(&rc);

	int x = rc.Width();
	int y = rc.Height();
	int n = 200;

	m_fifo.create(x, y, n, m_frame_num);
	m_pRead = (IRead *)&m_fifo;

	m_writer.start(&m_fifo, n);
}

void MyPaintWnd::OnPaint()
{
	CPaintDC dc(this);
	m_pRead->read(&dc);
}


