#include "stdafx.h"
#include "Writer.h"

unsigned __stdcall my_thread(void *pData)
{
	CWriter *p = (CWriter*)pData;
	p->work(); 
	return 0;
}

void CWriter::start(IWrite *fifo, int &count)
{
	m_dwTimeOut = 100;
	m_pFifo = fifo;
	m_count = count;
	m_hExitThread = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	m_hThread = (HANDLE)_beginthreadex(NULL, 0, my_thread, this, 0, 0);
}

void CWriter::work()
{
	while (true)
	{
		DWORD ret = ::WaitForSingleObject(m_hExitThread, m_dwTimeOut);

		if (ret == WAIT_TIMEOUT)
		{
			m_pFifo->write(m_count);
		}
		else if (ret == WAIT_OBJECT_0)
		{
			return;
		}
	}
}

void CWriter::stop()
{
	::SetEvent(m_hExitThread);
	::WaitForSingleObject(m_hThread, INFINITE);
	::CloseHandle(m_hThread);
	::CloseHandle(m_hExitThread);
}