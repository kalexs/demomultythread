#pragma once

#include "MyFifo.h"
#include "Writer.h"

// MyPaintWnd
class MyFifo;

class MyPaintWnd : public CWnd
{
	DECLARE_DYNAMIC(MyPaintWnd)

public:
	MyPaintWnd() { m_RED = 0; m_GREEN = 0; m_BLUE = 0; m_frame_num = 100; };
	virtual ~MyPaintWnd();
	BYTE m_RED;
	BYTE m_GREEN;
	BYTE m_BLUE;
	MyFifo m_fifo;
	IRead *m_pRead;
	CWriter m_writer;
	int m_wrspeed;
	int m_frame_num;

	void create(void);

protected:


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
};


